__State__: #SUPP_REQUIRED 

___Connections___: [[Carrier signal]] [[Sidebands]] [[Bandwidth]]
___Tags___:  #radio_signal 
___Sources___: [wikipedia](https://en.wikipedia.org/wiki/Amplitude_modulation) [stackexchange](https://dsp.stackexchange.com/questions/2284/why-are-sidebands-generated-in-am-and-fm) [quora](https://www.quora.com/Why-does-Amplitude-Modulation-have-sideband-frequencies-Shouldnt-the-carrier-wave-have-only-one-frequency)
___Not proceed sources___: 

---

### __General__
	AM - stands for amplitude modulation. This is a modulation technique for
	transimitting information over radio signal.

### __How modulation works__
	Depending on our transmitting bit is 0' or 1' we change the signal
	strength respectevly. If we want to send 1' we just simply increase signal
	amplitute if we want to send 0' we decrease signal amplitute.
	Here is an example:
	
	![[am_modulation_01.png]]

### __Features__ (SUPP)
	This modulation probably produces sidebands. As has been told it is equal
	to information signal frequency.